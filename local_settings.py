# coding: utf-8
LOCAL_SETTINGS = True
from settings import *
import os

DEBUG = os.environ.get('DEBUG', '') != ''

SENTRY_DSN = os.environ.get(
    'SENTRY_DSN',
    'https://4d24c79725a4sdfsdfrr43e09eb31a9e1e6c05fa:d0b6d1789f584055916328a959fdb1ed@app.getsentry.com/3425')

SECRET_KEY = os.environ.get(
    'SECRET_KEY',
    '^ir&3sdfgBKe0l5kkI&5)kL*2ZRAg9mN7e^&^*ePsDDaJ2!sHpkbz2m09imvvuCU')

GLOBAL_JSON_CONFIG = '/home/docker/code/conf/app-config.json'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ.get('POSTGRES_DB', 'pcart'),
        'USER': os.environ.get('POSTGRES_USER', 'pcart'),
        'PASSWORD': os.environ.get('POSTGRES_PASSWORD', 'qazwsx'),
        'HOST': os.environ.get('POSTGRES_HOST', '172.17.0.9'),
        'PORT': os.environ.get('POSTGRES_PORT', '5432'),
    },
}

MEDIA_ROOT = '/home/docker/code/media/'
MEDIA_URL = '/media/'
STATIC_ROOT = '/home/docker/code/static/'
STATIC_URL = '/static/'
LFS_LOG_FILE = '/home/docker/code/logs/pcart.log'


EMAIL_DEBUG = DEBUG
CONTACT_EMAIL = os.environ.get('CONTACT_EMAIL', 'sshop@the7bits.com')
EMAIL_HOST = os.environ.get('EMAIL_HOST', 'smtp.webfaction.com')
EMAIL_HOST_USER = os.environ.get('EMAIL_HOST_USER', 'drom_aquaweb')
EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_HOST_PASSWORD', '36ef87')
EMAIL_PORT = os.environ.get('EMAIL_PORT', '1025')
DEFAULT_FROM_EMAIL = os.environ.get(
    'DEFAULT_FROM_EMAIL', 'no-reply@the7bits.com')

COMPRESS_ENABLED = True
COMPRESS_OFFLINE = True

WSGI_RESTART_CODE = (
    'touch /home/docker/code/reload',
)

# Need to read external file for theme list
# SHOP_THEMES
AVAILABLE_SHOP_THEMES = (
    ('basic_theme', u'Basic theme'),
)
SHOP_THEME = None


# Old extention settings
ALLOW_REGIONS = True

SSHOP_USE_1C = True
USE_HR_URLS = True

PLUGINS_REPOSITORY_DEFAULT_URL = 'http://sitepanel.the7bits.com/'  # PLUGINS_REPOSITORY_DEFAULT_URL
PROJECT_IDENTIFIER = 'infoshina'  # PROJECT_IDENTIFIER
PROJECT_SECRET_CODE = '6nJXwT6p'  # PROJECT_SECRET_CODE


MIDDLEWARE_CLASSES = (
    'raven.contrib.django.middleware.SentryResponseErrorIdMiddleware',
    'django.middleware.cache.UpdateCacheMiddleware',
) + MIDDLEWARE_CLASSES + (
    'django.middleware.cache.FetchFromCacheMiddleware',
    #'raven.contrib.django.middleware.Sentry404CatchMiddleware',
)

TEMPLATE_LOADERS = (
    ('django.template.loaders.cached.Loader', (
        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
    )),
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'root': {
        'level': 'WARNING',
        'handlers': ['sentry'],
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'sentry': {
            'level': 'ERROR',
            'class': 'raven.contrib.django.handlers.SentryHandler',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
        'file': {
            # 'level': 'ERROR',
            'class': 'logging.FileHandler',
            'filename': os.path.join(PROJECT_DIR, 'error.log'),
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'django.db.backends': {
            'level': 'ERROR',
            'handlers': ['console'],
            'propagate': False,
        },
        'raven': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
        'sentry.errors': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
        'sshop': {
            'handlers': ['file'],
            # 'level': 'ERROR',
            'propagate': True,
        },
    },
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': 'unix:/tmp/memcached.sock',  # MEMCACHE_LOCATIONS (you can use separated by comma locations)
        'TIMEOUT': 2592000,  # CACHE_TIMEOUT
        'KEY_PREFIX': 'infoshina',  # CACHE_KEY_PREFIX
    }
}

CACHE_MIDDLEWARE_KEY_PREFIX = 'infoshina'  # CACHE_KEY_PREFIX

ROBOTS_USE_SITEMAP = False
LFS_INVOICE_EMAIL_REQUIRED = False

SSHOP_FTP_SOURCES = [
    {
        'DOMAIN': 'infoshina.com.ua',
        'USER': 'infoshina',
        'PASSWORD': 'shinapasswd',
        'THIS_SERVER': False,
        'DIR': '/home/webprod/projects/infoshina/1c/',
    },
    {
        'DOMAIN': 'ftp201.p-cart.com',
        'USER': 'infoshina',
        'PASSWORD': 'shinapasswd',
        'THIS_SERVER': False,
        'DIR': '',
    },
    {
        'DOMAIN': 'ftpserv.p-cart.com',
        'USER': 'infoshina',
        'PASSWORD': 'shinapasswd',
        'THIS_SERVER': False,
        'DIR': '',
    },
    {
        'DOMAIN': '144.76.76.188',
        'USER': 'infoshina',
        'PASSWORD': 'shinapasswd',
        'THIS_SERVER': False,
        'DIR': '',
    },
]

SSHOP_HTTP_SOURCES = [
    {
        'DOMAIN': 'infoshina.com.ua',
        'DIR': '/home/webprod/projects/infoshina/'
    },
]
SSHOP_PRICE_DIR = '/home/webprod/projects/infoshina/media/prices/'

FTS_LIVE_SEARCH_LIMIT = 10
FTS_SEARCH_LIMIT = 30
FTS_SEARCH_PRODUCT_FOR_NAME = True

PRICE_NAVIGATOR_TIMEPOINTS = ['11:00', '12:00', '15:00', '20:00']
SHOW_FILTER_COUNTERS = False

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'sshop_haystack.sshop_elasticsearch_backend.SshopElasticsearchEngine',
        'URL': 'http://10.0.0.254:9200/',
        'INDEX_NAME': 'infoshina',
        'INCLUDE_SPELLING': True,
    },
}

SSHOP_IMAGE_SIZES = {
    'small': (60, 60),
    'medium': (100, 100),
    'large': (200, 200),
    'huge': (500, 500),
}

LOGIN_URL = "/login"


if DEBUG:
    INTERNAL_IPS = ('127.0.0.1')

    MIDDLEWARE_CLASSES += (
        'debug_toolbar.middleware.DebugToolbarMiddleware',
        'sshop.middleware.ProfileMiddleware',
        'lfs.utils.middleware.ProfileMiddleware',
        'sshop.middleware.memoryMiddleware',
        # 'sshop.middleware.LoggingMiddleware',
    )

    INSTALLED_APPS += (
        'debug_toolbar',
    )

    DEBUG_TOOLBAR_PANELS = (
        'debug_toolbar.panels.version.VersionDebugPanel',
        'debug_toolbar.panels.timer.TimerDebugPanel',
        'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
        'debug_toolbar.panels.headers.HeaderDebugPanel',
        'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
        'debug_toolbar.panels.template.TemplateDebugPanel',
        'debug_toolbar.panels.sql.SQLDebugPanel',
        'debug_toolbar.panels.signals.SignalDebugPanel',
        'debug_toolbar.panels.logger.LoggingPanel',
    )

    DEBUG_TOOLBAR_CONFIG = {
        'INTERCEPT_REDIRECTS': False,
    }

import sys
if 'test' not in sys.argv:
    from adminconfig.utils import JSONConfigFile
    json_config = JSONConfigFile(GLOBAL_JSON_CONFIG)
    json_config.get_full_config()
    for block_name in json_config.config.keys():
        block = json_config.config[block_name]
        for i in block.keys():
            globals()[i] = block[i]

INSTALLED_APPS = (
    'raven.contrib.django',
) + INSTALLED_APPS

if SHOP_THEME is not None and SHOP_THEME != "basic_theme":
    INSTALLED_APPS = (
        SHOP_THEME,
    ) + INSTALLED_APPS


if DEBUG:
    # Ignore collectstatic job when debug
    TASK_NAME_EXCLUDES = [
        'sbits_plugins.jobs.collectstatic_job',
    ]

if 'test' in sys.argv:
    # Test environment
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': ':memory:',
            # 'TEST_NAME': 'sshop.sqlite',
        },
    }
    # Ignore collectstatic job when testing
    TASK_NAME_EXCLUDES = [
        'sbits_plugins.jobs.collectstatic_job',
    ]
    # This is necessary for running jobs immideatly after shedule
    ACTIVE_TASK_MANAGER = 'tasks.api.DummyTaskQueue'
