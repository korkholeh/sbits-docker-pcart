from ubuntu

maintainer korkholeh@gmail.com

# Update the default application repository sources list
run echo "deb-src http://archive.ubuntu.com/ubuntu precise main universe" >> /etc/apt/sources.list
run echo "deb-src http://archive.ubuntu.com/ubuntu precise-updates main universe" >> /etc/apt/sources.list
run echo "deb-src http://archive.ubuntu.com/ubuntu precise-security main universe" >> /etc/apt/sources.list
run apt-get update

run locale-gen uk_UA
run locale-gen uk_UA.UTF-8
run locale-gen ru_RU
run locale-gen ru_RU.UTF-8
run update-locale

run apt-get install -y build-essential git wget mc psmisc
run apt-get install -y python python-dev python-setuptools
run apt-get install -y libpq-dev postgresql-server-dev-all
run apt-get install -y cython libxml2-dev libxslt-dev
run apt-get install -y libmagickwand-dev
run apt-get install -y nginx supervisor
run easy_install pip

run pip install uwsgi
run apt-get install -y python-software-properties
run add-apt-repository -y ppa:nginx/stable
run apt-get update
run apt-get install -y nginx
run apt-get build-dep -y python-imaging
run ln -s /usr/lib/`uname -i`-linux-gnu/libfreetype.so /usr/lib/
run ln -s /usr/lib/`uname -i`-linux-gnu/libjpeg.so /usr/lib/
run ln -s /usr/lib/`uname -i`-linux-gnu/libz.so /usr/lib/
run pip install --pre pillow-pil

add . /home/docker/code/

run echo "daemon off;" >> /etc/nginx/nginx.conf
run rm /etc/nginx/sites-enabled/default
run ln -s /home/docker/code/nginx-app.conf /etc/nginx/sites-enabled/
run ln -s /home/docker/code/supervisor-app.conf /etc/supervisor/conf.d/

run cd /home/docker/
run mkdir src
run cd src
run wget http://nodejs.org/dist/v0.10.26/node-v0.10.26.tar.gz
run tar xzvf node-v0.10.26.tar.gz
run cd node-v0.10.26
run ./configure
run make
run make install
run npm install -g less
run npm install -g coffee-script
run npm install -g stylus

run cd /home/docker/src/
# For test only
run wget -O sshop.tar.gz https://bitbucket.org/the7bits/sshop/get/opt-030.tar.gz
run mkdir /home/docker/code/app
# Need to unpack without subdirectory
run tar xzvf sshop.tar.gz -C /home/docker/code/app

# Need to remove PIL and sbits-plugins from the file firstly
run pip install -r /home/docker/code/app/requirements.txt

run mkdir /home/docker/plugins/
run export PYTHONPATH=$PYTHONPATH:/home/docker/plugins/lib/python2.7/site-packages/
# For test only
run pip install --install-option="--prefix=/home/docker/plugins/" http://sitepanel.the7bits.com/media/repo/944c9364-42cf-44bb-9cc9-a1adbb11f9a9/sbits_plugins-3.3.tar.gz

# There your need to create local_settings.py

run cd /home/docker/code/app/
# Need to ignore admin creation
run python manage.py syncdb --all
run python manage.py migrate --fake
run python manage.py lfs_init
run python manage.py collectstatic --noinput
run python manage.py compress

run touch /home/docker/code/reload
run cd /home/docker/code/

expose 80
cmd ["supervisord", "-n"]
